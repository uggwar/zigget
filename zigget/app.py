""" Download the latest zig master for amd64 """
import os
import subprocess
import requests
from bs4 import BeautifulSoup, Tag

INSTALLDIR = os.path.join(os.environ["HOME"], "local")
TMPDIR = os.path.join(os.environ["HOME"], "tmp")
URL = "https://ziglang.org/download/"

def run():
    """ Do everything """
    page = requests.get(URL, timeout=10)
    soup = BeautifulSoup(page.content, "html.parser")
    results = soup.find(id="content")
    if isinstance(results, Tag):
        links = results.find_all("a", string=lambda text: "linux" in text.lower())
        for link in links:
            if "linux" in link.text and "x86_64" in link.text:
                if "builds" in link["href"]:
                    response = requests.get(link["href"], timeout=60)
                    filename = os.path.join(TMPDIR, link.text)
                    with open(filename, "wb") as output:
                        output.write(response.content)
                    subprocess.run(
                        ["tar", "xf", filename, "-C", INSTALLDIR], check=False
                    )
                    symlink_source = os.path.join(INSTALLDIR, link.text[:-7])
                    symlink_destination = os.path.join(INSTALLDIR, "zig")
                    if os.path.exists(symlink_destination):
                        os.remove(symlink_destination)
                    os.symlink(symlink_source, symlink_destination)
                    os.remove(os.path.join(TMPDIR, link.text))
                    print("Installed: " + link.text)
